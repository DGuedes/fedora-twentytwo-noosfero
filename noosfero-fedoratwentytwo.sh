#!/bin/bash

if [ $# -eq 0 ]; then
  echo "You don't picked an installation. Please, choose one and try again:"
  echo "portal"
  echo "Command: ./noosfero-fedoratwentytwo.sh portal"
  echo "noosfero"
  echo "Command: ./noosfero-fedoratwentyone.sh noosfero"
  echo "To clone, use clone as second argument. Example:"
  echo "./noosfero-fedoratwentytwo portal clone"

else
  if [ $1 == "portal" ]; then
    if [ $2 == "clone" ]; then
      echo "Clonning"
      echo "git clone https://gitlab.com/unb-gama/noosfero.git"
      echo "...."
    fi

    echo "Portal installation selected."
    echo "If any command doesn't run well, try to run it manually."

    git clone https://gitlab.com/unb-gama/noosfero.git
    echo "Please, if asked, accept the key."
    echo "gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3"
    echo "...."
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    echo "Installing rvm."
    echo "\curl -sSL https://get.rvm.io | bash"
    echo "....."
    \curl -sSL https://get.rvm.io | bash
    echo "rvm installed."
    echo "...."
    echo "source ~/.rvm/scripts/rvm"
    source ~/.rvm/scripts/rvm
    echo "Installing Ruby 1.9.3"
    echo "rvm install 1.9.3"
    echo "OBS: Your root pass will be asked several times. Be warned."
    echo "....."
    rvm install 1.9.3
    echo "Installing postgresql"
    echo "sudo dnf install postgresql"
    echo "...."
    sudo dnf install postgresql
    echo "Installing postgresql-devel"
    echo "sudo dnf install postgresql-devel"
    echo "...."
    sudo dnf install postgresql-devel
    echo "Installing postgresql-server and postgresql-contrib"
    sudo dnf install postgresql-server postgresql-contrib
    echo "Enabling postgresql"
    echo "sudo systemctl enable postgresql"
    sudo systemctl enable postgresql
    echo "initdb setup:"
    echo "sudo postgresql-setup initdb"
    sudo postgresql-setup initdb
    echo "sudo systemctl start postgresql"
    sudo systemctl start postgresql
    echo "Installing rails 3.2.22"
    echo "gem install rails -v '3.2.22'"
    gem install rails -v '3.2.22'
    echo "Installing dependencies"
    echo "sudo dnf install tango-icon-theme"
    sudo dnf install tango-icon-theme
    echo "sudo dnf install libxml2-devel libxslt libxslt-devel"
    sudo dnf install libxml2-devel libxslt libxslt-devel
    echo "sudo dnf instal ImageMagick"
    sudo dnf install ImageMagick
    echo "ImageMagick-devel"
    sudo dnf install ImageMagick-devel
    echo "Now, installing nodejs. OBS: If you don't want to install nodejs with sudo,
     stop this installation now and install it by yourself."
    echo "sudo dnf install nodejs"
    sudo dnf install nodejs
    echo "Running bundle"
    echo "bundle install"
    bundle install
    echo "Running script/quick-start"
    ./script/quick-start
    echo "obs: If anything doesn't looks right, remove config/database.yml and run
      quick-start again"
  elif [ $1 == "noosfero" ]; then
    if [ $2 == "clone" ]; then
      echo "Clonning"
      echo "git clone https://gitlab.com/unb-gama/noosfero.git"
      echo "...."
    fi
    echo "Main noosfero selected."
  else
    echo "Invalid option."
  fi
fi
